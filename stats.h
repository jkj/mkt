/**
 * Copyright (c) 2015 Voidware Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * 
 * contact@voidware.com
 */

#ifndef __stats_h__
#define __stats_h__

#include <iostream>
#include <math.h>

struct Stats
{
    // * Collect statistics and calculate the mean, variance and peak.

    Stats() { reset(); }

    void                reset()
    {
        _n = 0;
        _mean = 0;
        _varSum = 0;
        clearPeak();
    }

    /// Add a new sample
    void                add(double x)
    {
        _updateMin(x);
        _updateMax(x);

        double dx = x - _mean;
        ++_n;
        if (dx)
        {
            _mean += dx/_n;
            if (_n > 1) _varSum += dx*(x - _mean);
        }
        _updatePeak(x);
    }
    
    /// Remove a sample.
    void                remove(double x)
    {
        if (_n > 1)
        {
            double t = x - _mean;
            _mean -= t/--_n;
            _varSum -= (x - _mean)*t;
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const Stats& si)
    {
        os << "(mean:" << si.mean()
           << " std:" << si.stdDeviation()
           << " peak:" << si.peak()
           << " min:" << si.minimum()
           << " max:" << si.maximum()
           << ")";
        return os;
    }

    /// Return the number of sampes collected.
    unsigned int        count() const { return _n; }

    /// Return the mean of the samples collected.
    double              mean() const { return _mean; }

    /// Return the variance of the samples collected.
    double              variance() const
    { return _n > 1 ? _varSum/(_n-1) : 0; }

    /// Return the standard deviation of the samples collected.
    double              stdDeviation() const { return sqrt(variance()); }

    /// Return the peak absolute value of the samples collected.
    double              peak() const { return _peak; }

    /// Return the maximum value observed.
    double              maximum() const { return _max; }

    /// Return the minimum value.
    double              minimum() const { return _min; }

    /// Reset the peaks.
    void                clearPeak()
    {
        _peak = 0; 
        _min = 0;
        _max = 0;
    }

    bool                isAPeak(double v) const
    {
        return v == _peak || v == _max || v == _min;
    }

private:

    void                _updatePeak(double x)
    {
        if (x >= 0) { if (x > _peak) _peak = x; }
        else { if (-x > _peak) _peak = -x; }
    }

    void                _updateMin(double x)
    { if (x < _min || !_n)  _min = x; }

    void                _updateMax(double x)
    { if (x > _max || !_n) _max = x; }

    unsigned int        _n;
    double              _mean;
    double              _varSum;
    double              _peak;
    double              _min;
    double              _max;
};

#endif // __stats_h__


