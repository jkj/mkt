/**
 *
 *    _    __        _      __                           
 *   | |  / /____   (_)____/ /_      __ ____ _ _____ ___ 
 *   | | / // __ \ / // __  /| | /| / // __ `// ___// _ \
 *   | |/ // /_/ // // /_/ / | |/ |/ // /_/ // /   /  __/
 *   |___/ \____//_/ \__,_/  |__/|__/ \__,_//_/    \___/ 
 *                                                       
 *  Copyright (�) Voidware 2021.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 * 
 *  contact@voidware.com
 */

//Mann Kendall Test

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <ctype.h>
#include <assert.h>

#include <math.h>
#include "stats.h"

static inline double normal(double x, double xmean, double sigma)
{
    return (erf((x - xmean)/sigma/sqrt(2)) + 1)/2;
}

struct MKT
{
    typedef std::vector<double> Vals;

    Stats       _stats;
    Vals        _vals;
    double      _e = 0.0001;
    

    int  size() const { return _vals.size(); }

    void read(FILE* fp)
    {
        char line[255];

        for (;;)
        {
            *line = 0;
            fgets(line, sizeof(line), fp);

            char* p = line;
            if (!*p) break;
            
            while (isspace(*p)) ++p;
            if (!*p) continue; // blank line

            double v = atof(p);

            _vals.push_back(v);
            _stats.add(v);
        }
    }

    double sign(int j, int k)
    {
        double a = _vals[j];
        double b = _vals[k];
        double d = a - b;
        if (d > _e) return 1;
        if (d < -_e) return -1;
        return 0;
    }

    double s()
    {
        int t = 0;
        int n = size();
        for (int k = 0; k < n-1; ++k)
            for (int j = k+1; j < n; ++j) t += sign(j, k);

        return t;
    }

    double vars()
    {
        int n = size();
        if (!n) return 0;
        
        std::sort(_vals.begin(), _vals.end());

        // size of each tie group
        std::vector<int> tp;
        
        // find number of tied groups and size of each group
        int g = 0;
        double last = _vals[0];
        bool same = false;
        for (int i = 1; i < n; ++i)
        {
            if (abs(_vals[i] - last) < _e)
            {
                if (!same)
                {
                    ++g;

                    // new group with 2 elt to start
                    tp.push_back(2);

                    same = true;
                }
                else
                {
                    // still the same, so increase group count
                    ++tp[g-1];
                }
            }
            else same = false;
            last = _vals[i];
        }

        printf("tie groups: %d\n", g);
        //for (int i = 0; i < g; ++i) printf("tie(%d) = %d\n", i, tp[i]);

        // tie group adjustment
        int tadj = 0;

        for (int i = 0; i < g; ++i)
        {
            int ni = tp[i];
            tadj += ni*(ni-1)*(2*ni+5);
        }

        return ((n*(n-1)*(2*n+5))-tadj)/18.0;
    }
    
};

void report(MKT& mkt)
{
    printf("Mann Kendall, data size: %d\n", mkt.size());

    // emit some basic stats
    std::cout << mkt._stats << std::endl;
    
    double s = mkt.s();
    printf("s = %f\n", s);

    double vs = mkt.vars();
    printf("var(s) = %f (%f)\n", vs, sqrt(vs));

    double z = 0;
    if (s > 0) z = (s-1)/sqrt(vs);
    else if (s < 0) z = (s+1)/sqrt(vs);        
    printf("z = %f\n", z);

    double p = 2*normal(-abs(z), 0, 1);
    printf("prob = %f\n", p);

    if (p < 0.05)
    {
        printf("Test shows ");
        if (s >= 0) printf("increasing");
        else printf("decreasing");
        printf(" trend.\n");
    }
    else
    {
        printf("Test shows NO ");
        if (s >= 0) printf("increasing");
        else printf("decreasing");
        printf(" trend.\n");
    }
}

int main(int argc, char** argv)
{
    const char* fname = 0;
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
        }
        else fname = argv[i];
    }

    if (!fname)
    {
        printf("Usage: %s <file.txt>\n", argv[0]);
        return -1;
    }

    FILE* fin = fopen(fname, "r");
    if (fin)
    {
        MKT mkt;
        mkt.read(fin);

        report(mkt);
        
        fclose(fin);
    }
    else
    {
        printf("Can't open file '%s'\n", fname);
    }
    
    return 0;
}

